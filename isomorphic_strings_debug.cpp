// isomorphic strings (debug)
// firecode.io
// one map, two sets, two loops

#include <iostream>
#include <unordered_map>
#include <unordered_set>

void displayMap(std::unordered_map<char, char> mp) {
    std::cout << "Correct mapping..." << std::endl;
    for (auto ch : mp)
      std::cout << ch.first << " -> " << ch.second << std::endl;
    std::cout << std::endl;
}

bool isIsomorphic(std::string input1, std::string input2) {
    // for debug purposes
    std::cout << "First string : " << input1 << std::endl;
    std::cout << "Second string: " << input2 << std::endl;
    // not equal strings cannot be isomorphic
    if (input1.size() != input2.size())
        return false;
    // empty strings are ispotropic
    if (input1.empty() && input2.empty())
        return true;
    // mapping all unique characters to each other
    std::unordered_map<char, char> iso;
    std::unordered_set<char> set1; // holds unique keys
    std::unordered_set<char> set2; // holds unique keys
    auto ch1 = input1.begin();
    auto ch2 = input2.begin();
    for (ch1; ch1 != input1.end(); ++ch1, ++ch2)
        if (set1.find(*ch1) == set1.end() && set2.find(*ch2) == set2.end()) {
            set1.insert(*ch1);
            set2.insert(*ch2);
            iso.insert({*ch1, *ch2}); // insert only unique values
        }
    // for debug purposes
    displayMap(iso);
    // creating correctly mapped string
    std::string output;
    for (auto ch : input1)
        if (auto it = iso.find(ch); it != iso.end()) // C++17
            output += it->second;
    // for debug purposes
    std::cout << "Mapped output: " << output << std::endl;
    std::cout << "Second string: " << input2 << std::endl;
    // comparing if the sedond string is the same as mapped
    if (output != input2)
        return false;
    return true;
}

int main() {
    // not isomorphic
    std::string str1 = "abcz";
    std::string str2 = "abca";
    // isomorphic
    std::string str3 = "abcabc";
    std::string str4 = "xyzxyz";
    // not isomorphic
    std::string str5 = "abcabc";
    std::string str6 = "xyzxyy";
    // ispotropic
    std::string str7 = "abacdea";
    std::string str8 = "zbzcdvz";

    if (isIsomorphic(str1, str2))
        std::cout << "Isomorphic" << std::endl;
    else
        std::cout << "NOT isomorphic" << std::endl;

    return 0;
}
