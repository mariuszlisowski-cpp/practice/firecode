// palindrome tester
// firecode.io

#include <iostream>
#include <algorithm> // transform, reverse

// SOLUTION START
bool isPalindrome(std::string str) {
    if (str.empty())
        return true;
    std::transform(str.begin(), str.end(), str.begin(), ::toupper);
    std::string rev = str;
    std::reverse(std::begin(rev), std::end(rev));
    return (str == rev);
}
// SOLUTION END

int main() {
    std::string s("racecar");

    if (isPalindrome(s))
        std::cout << s << " is a palindrome" << std::endl;
    else
        std::cout << s << " is NOT a palindrome" << std::endl;

    return 0;
}
