// permutations
// ~ have no meaning

#include <iostream>
#include <map>
#include <string>

bool permutation(std::string input1, std::string input2) {
    if (input1.empty() && input2.empty())
        return true;
    if (input1.size() != input2.size())
        return false;
        
    std::map<char, int> mch1;
    std::map<char, int> mch2;
    size_t count1{}, count2{};
    for (const char& ch : input1) {
        ++mch1[ch];
        count1 += mch1.count(ch);
    }
    for (const char& ch : input2) {
        ++mch2[ch];
        count2 += mch1.count(ch);
    }
    
    return count1 == count2;
}

int main() {
    std::string str1{ "hello" };
    std::string str2{ "elloh" };

    if (permutation(str1, str2))
        std::cout << "Permutation\n";
    else
        std::cout << "NOT permutation\n";
        
    return 0;
}