//

#include <iostream>
#include <unordered_map>

// SOLUTION START
bool are_all_characters_unique(std::string str) {
    std::unordered_map<char, int> mp;
    for (auto ch : str) {
        mp[ch]++;
        if (mp[ch] > 1)
            return false;
    }
    return true;
}
// SOLUTION END

int main() {
    std::string s {"!qwerty!"};

    if (are_all_characters_unique(s))
        std::cout << "No twins!" << std::endl;
    else
        std::cout << "Repeated characters found!" << std::endl;

    return 0;
}
