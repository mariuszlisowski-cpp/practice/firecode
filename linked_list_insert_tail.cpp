// insert a node at the back of a linked list
// firecode.io

#include <iostream>

struct listNode {
public:
    int value;
    listNode * next;
};

listNode* insert_at_tail(listNode* head, int data) {
    listNode* node = new listNode {data, nullptr};
    if (!head)
        head = node;
    else {
        listNode* last = head;
        while (last->next)
            last = last->next;
        last->next = node;
    }
    return head;
}

void displayList(listNode * head) {
    while (head) {
        std::cout << head->value << " -> ";
        head = head->next;
    }
    std::cout << "nullptr" << std::endl;
}

int main() {
    listNode * head = nullptr;

    head = insert_at_tail(head, 10);
    head = insert_at_tail(head, 20);
    head = insert_at_tail(head, 30);

    displayList(head);

    return 0;
}
