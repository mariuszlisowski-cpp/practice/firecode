// palindrome tester

#include <iostream>
#include <string>

bool is_string_palindrome(std::string str) {
    if (!str.empty()) {
        auto first = str.begin();
        auto last = str.end() - 1;
        do {
            std::cout << *first << " : " << *last << '\n';
            if (*first != *last)
                return false;
        } while (++first < --last);
    }
    return true;
}

int main() {
    std::string s{ "" };
    // std::string s{ "rota!@ator" };

    if (is_string_palindrome(s))
        std::cout << "Palindrome\n";
    else
        std::cout << "NOT palindrome\n";

    return 0;
}