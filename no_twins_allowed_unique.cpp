// 

#include <iostream>
#include <algorithm>
#include <string>

bool are_all_characters_unique(std::string str) {
    if (str.empty())
        return true;
    std::sort(std::begin(str), std::end(str));
    std::string test = str;
    test.erase(std::unique(std::begin(test), std::end(test)),
                std::end(test));
    return str == test ? true : false;
}

int main() {
    std::string s {"qwerty"};

    if (are_all_characters_unique(s))
        std::cout << "All unique" << std::endl;
    else
        std::cout << "NOT unique" << std::endl;
    
    return 0;
}