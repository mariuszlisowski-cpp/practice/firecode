// merge_intervals 

#include <algorithm>
#include <iostream>
#include <vector>

class Interval {
public:
    int start;
    int end;
    Interval(){}
    Interval(int start, int end) {
        this->start = start;
        this->end = end;
    }
};

std::vector<Interval> merge_intervals(std::vector<Interval> intervals_list) {
    std::vector<Interval> merged_intervals;
    if (!intervals_list.empty()) {
        std::sort(intervals_list.begin(),
                intervals_list.end(),
                [](Interval interA, Interval interB) {
                    return interA.start < interB.start;
                });
        Interval prev = intervals_list.at(0);
        for (auto it = intervals_list.begin() + 1; it != intervals_list.end(); it++) {
            Interval curr = *it;
            if (curr.start <= prev.end) {
                prev = { prev.start, std::max(curr.end, prev.end) };
            } else {
                merged_intervals.push_back(prev);
                prev = curr;
            }
        }
        merged_intervals.push_back(prev);
    }

    return merged_intervals;
}

int main() {
    std::vector<Interval> intervals {
        { 1, 3},
        { 2, 6},
        { 8, 10},               // [1,6], [8,10], [15,18]
        // { 5, 10},               // [1,10], [15,18]
        {15, 18}
    };

    std::vector<Interval> merged = merge_intervals(intervals);

    for (auto&& interval : merged) {
        std::cout << interval.start << ", " << interval.end << std::endl;
    }

    return 0;
}
