// insert node at the front of a linked list

#include <iostream>

struct listNode {
    int value;
    listNode* next;
};

listNode* insert_at_head(listNode* head, int data) {
    return new listNode{ data, head };
}

void traverse(listNode* root) {
    while (root) {
        std::cout << root->value << " -> ";
        root = root->next;
    }
}

int main() {
    listNode* head = nullptr;

    head = insert_at_head(head, 10);
    head = insert_at_head(head, 20);
    head = insert_at_head(head, 30);

    traverse(head);

    return 0;
}