// max_profit 

#include <iostream>

// simply add to the profit the difference of all ascending pairs in the sequence
int max_profit(int prices[], int sz) {
    int profit{};
    for (int i = 0; i < sz - 1; i += 2) {
        int transaction = prices[i + 1] - prices[i];
        if (transaction >= 0) {
            profit += transaction;
        }
    }

    return profit;
}

int main() {
    int array[] { 0, 50, 10, 100, 30 };     // 140 = 50 + 90
    // int array[] { 0, 100, 0, 100, 0, 100 }; // 300 = 100 + 100 + 100
    // int array[] { 100, 100, 80, 20 };       // 0
    // int array[] { 50, 100, 20, 80, 20 };    // 110 = 50 + 60
    // int array[] { 100, 40, 20, 10};         // 0
    // int array[] { 1, 1};                    // 0
    // int array[] {};                         // 0
    int size = sizeof(array) / sizeof(*array);

    std::cout << max_profit(array, size) << std::endl;

    return 0;
}
