// binary search - integer array

#include <iostream>
// #include <vector>
// #include <algorithm>

/// SOLUTION START  //////////////////////////////////////////////////////////
bool binary_search(int arr[], int size, int n) {
    if (size > 0) {
        int left{}, right = size - 1, middle;
        while (left <= right) {
            middle = left + (right - left) / 2;
            if (n == arr[middle])
                return true;
            else if (n > arr[middle])
                left = middle + 1;
            else
                right = middle - 1;
        }
    }
    return false;
}
/// SOLUTION END ////////////////////////////////////////////////////////////

int main() {
    int arr[]{ 2, 5, 6, 8, 9 };
    int size = sizeof(arr) / sizeof(*arr);

    int x = 9;
    if (binary_search(arr, size, x))
        std::cout << "Found\n";
    else
        std::cout << "NOT found\n";

    return 0;
}