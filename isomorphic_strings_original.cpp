// isomorphic strings (original solution)
// firecode.io
// INCORRECT!

#include <iostream>
#include <map>
#include <unordered_set>

using namespace std;

void displayMap(map<char, int> mp) {
    cout << "Correct mapping..." << endl;
    for (auto ch : mp)
      cout << ch.first << " -> " << ch.second << endl;
    cout << endl;
}

bool is_isomorphic(string input1, string input2) {
    if (input1.length() != input2.length())
        return false;

    cout << "First string : " << input1 << endl;
    cout << "Second string: " << input2 << endl;

    map<char, int> hm1;
    map<char, int> hm2;
    map<char, int>::iterator it1;
    map<char, int>::iterator it2;
    int val1;
    for (int i = 0; i < input1.length(); i++) {
        char c1 = input1.at(i);
        it1 = hm1.find(c1);
        if (it1 != hm1.end())
            it1->second += 1;
        else
            hm1.insert(pair<char, int>(c1, 1));

        displayMap(hm1);

        char c2 = input2.at(i);
        it2 = hm2.find(c2);
        if (it2 != hm2.end())
            it2->second += 1;
        else
            hm2.insert(pair<char,int>(c2, 1));

        displayMap(hm2);

        it1 = hm1.find(c1);
        it2 = hm2.find(c2);
        if (it1->second != it2->second)
            return false;
    }
    return true;
}

int main() {
    // not isomorphic
    std::string str1 = "abca";
    std::string str2 = "zbcc";
    // isomorphic
    std::string str3 = "abcabc";
    std::string str4 = "xyzxyz";
    // not isomorphic
    std::string str5 = "abcabc";
    std::string str6 = "xyzxyy";
    // ispotropic
    std::string str7 = "abacdea";
    std::string str8 = "zbzcdvz";

    if (is_isomorphic(str1, str2))
        cout << "Isomorphic" << endl;
    else
        cout << "NOT isomorphic" << endl;

    return 0;
}
