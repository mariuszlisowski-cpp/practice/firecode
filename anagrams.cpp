// anagrams

#include <algorithm>
#include <iostream>

bool is_anagram(std::string input1, std::string input2) {
    std::sort(input1.begin(), input1.end());
    std::sort(input2.begin(), input2.end());

    return input1 == input2;
}

int main() {
    std::string s1{ "abc" };
    std::string s2{ "ca" };

    if (is_anagram(s1, s2))
        std::cout << "Anagram\n";
    else
        std::cout << "NOT anagram\n";

    return 0;
}